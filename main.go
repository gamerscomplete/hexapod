/*
Configuration
	Determine position for all arms so that zeroes and offsets can be set
	Determine min/max angle for joints
		This could vary for the coxa joints so would need to be handled differently than the tibia and arm
*/

package main

import (
	"flag"
	"fmt"
	"github.com/jacobsa/go-serial/serial"
	log "github.com/sirupsen/logrus"
	//	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/lx16a"
	//	"io"
	"io/ioutil"
)

const (
	NUM_JOINTS = 3
	NUM_LEGS   = 6
)

var (
	serialPort = flag.String("serial-port", "/dev/ttyUSB0", "path to the serial port")
)

func main() {
	//Set starting position for wake
	//control: forward
	//gait tripod
	// 6, 2, 4
	// 1, 3, 5
	//set min and max of percentages of throw on each angle
	//coxa min -30
	//coxa max 30
	flag.Parse()

	sOpts := serial.OpenOptions{
		PortName:              *serialPort,
		BaudRate:              115200,
		DataBits:              8,
		StopBits:              1,
		MinimumReadSize:       0,
		InterCharacterTimeout: 100,
	}

	fmt.Println("opening serial")
	srl, err := serial.Open(sOpts)
	if err != nil {
		log.Fatalf("error opening serial port: %s\n", err)
	}
	defer srl.Close()
	var b []byte
	log.Info("purging serial buffer")
	b, err = ioutil.ReadAll(srl)
	if err != nil {
		log.Fatalf("error purging serial buffer: %s\n", err)
	}
	log.Infof("purged %d bytes", len(b))

	fmt.Println("new network")
	network := lx16a.NewNetwork(srl)

	hexapod := NewHexapod(network)

	hexapod.srl = srl

	//TODO: start network server
	if err := hexapod.setupServerConn(); err != nil {
		fmt.Println("Failed to setup server conn:", err)
		return
	}

	hexapod.Startup()

	hexapod.startLoop()
}
