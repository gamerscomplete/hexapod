/*
    right front & back, left middle legs touching the ground
    down legs move back, front legs move forward
    weight shifted to the other tripod
    down legs move back, front legs move forward

	Needed:
		leg down position
		leg up position

		how to change the height of the body

*/

package main

import (
	"time"
)

type TripodGait struct {
	transitionTime time.Duration
	poses          []Pose
	lastUpdate     time.Time
}

func (gait *TripodGait) Update() {

}

func NewTripodGait() *TripodGait {
	return &TripodGait{}
}

//func (gait *TripodGait)
