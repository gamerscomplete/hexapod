package main

import (
//	"time"
)

type Pose struct {
	//[appendage number][joint]angle in degrees
	legs [][]float32
}

type Gait interface {
	Update()
}

func NewPose() *Pose {
	pose := Pose{legs: make([][]float32, 6)}
	for k, _ := range pose.legs {
		pose.legs[k] = make([]float32, 3)
	}
	return &pose
}

func StaticPosPose(coxa float32, femur float32, tibia float32) Pose {
	pose := Pose{legs: make([][]float32, 6)}
	for k, _ := range pose.legs {
		pose.legs[k] = make([]float32, 3)
		pose.legs[k][0] = coxa
		pose.legs[k][1] = femur
		pose.legs[k][2] = tibia
	}
	return pose
}

var NeutralPose = StaticPosPose(90, 90, 180)

/*
Pose{
	//leg 1
	legs: [][]float32{
		[]float32{
			90,
			90,
			180,
		},
		//leg 2
		[]float32{
			90,
			90,
			180,
		},
		//leg 3
		[]float32{
			90,
			90,
			180,
		},
		//leg 4
		[]float32{
			90,
			90,
			180,
		},
		//leg 5
		[]float32{
			90,
			90,
			180,
		},
		//leg 6
		[]float32{
			90,
			90,
			180,
		},
	},
}
*/
