package main

import (
	"time"
)

func NewForwardAnimation() *Animation {
	animation := NewAnimation(10 * time.Second)
	//Lift first 3 legs
	animation.AddPose(
		Pose{
			legs: [][]float32{
				//leg 1
				[]float32{
					90,
					45,
					180,
				},
				//leg 2
				[]float32{
					90,
					90,
					180,
				},
				//leg 3
				[]float32{
					90,
					45,
					180,
				},
				//leg 4
				[]float32{
					90,
					90,
					180,
				},
				//leg 5
				[]float32{
					90,
					45,
					180,
				},
				//leg 6
				[]float32{
					90,
					90,
					180,
				},
			},
		},
	)
	//set legs back down
	animation.AddPose(NeutralPose)
	//lift other 3 legs forward
	animation.AddPose(
		Pose{
			legs: [][]float32{
				//leg 1
				[]float32{
					90,
					90,
					180,
				},
				//leg 2
				[]float32{
					90,
					45,
					180,
				},
				//leg 3
				[]float32{
					90,
					90,
					180,
				},
				//leg 4
				[]float32{
					90,
					45,
					180,
				},
				//leg 5
				[]float32{
					90,
					90,
					180,
				},
				//leg 6
				[]float32{
					90,
					45,
					180,
				},
			},
		})

	return animation
}
