package botclient

import (
	"gitlab.com/gamerscomplete/lx16a"
)

type SystemStats struct {
	BatteryVoltage float32
	BatteryDraw    float32
	Temp           float32
}

type ServoStatsRequest struct {
	// [arm]joint number
	Appendage int
	Joint     int
}

type ServoStats struct {
	ID      uint8
	Temp    int
	Voltage int
	Angle   float32
	//	Mode int
	MotorEnabled bool
	ServoError   lx16a.ServoError
}

type SetServoAngleRequest struct {
	Angle float32
	Time  uint16
}

type EnableServoRequest struct {
	Enable bool
}

//[arm][joint]position
type SetPose struct {
	Pose [][]float32
}

type ServoConfiguration struct {
	Configuration [][]uint8
}
