set -e
echo "Building hexapod"
mkdir -p modules/hexapod
go build -o modules/hexapod/hexapod .
echo "Copying genesis"
rsync -av ../CommandControl/genesis/genesis .

mkdir -p modules/wifi
rsync -av ../wifi/wifi modules/wifi/wifi
