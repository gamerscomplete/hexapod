package main

import (
	"fmt"
	"math"
	"time"
)

type Animation struct {
	keyframes   []Pose
	duration    time.Duration
	lastUpdate  time.Time
	currentStep int
}

func NewAnimation(duration time.Duration) *Animation {
	return &Animation{
		duration: duration,
	}
}

func (animation *Animation) AddPose(pose Pose) {
	animation.keyframes = append(animation.keyframes, pose)
}

func (animation *Animation) Update(delta time.Duration, speed float32) *Pose {
	keyframeSpacing := 100 / (len(animation.keyframes) + 1)

	//Check if the time passed is longer than the duration of the animation. This should only happen if something locks up the animation
	if delta > animation.duration {
		fmt.Println("length overload Delta:", delta, "animation duration:", animation.duration)
		return animation.Update(delta-animation.duration, speed)
	}

	//calculate the number of steps to progress
	steps := float32(animation.duration / delta)
	steps = steps * speed

	if steps > 100 {
		fmt.Println("steps is overshooting by a complete span. steps:", steps)
	}
	//check for rollover
	//using a loop instead of a simple if statement incase the steps is larger than 2 full steps
	for {
		if steps+float32(animation.currentStep) > 100 {
			fmt.Println("Correcting steps")
			steps = (steps + float32(animation.currentStep)) - 100
			animation.currentStep = 0
		} else {
			break
		}
	}

	thisStep := int(steps) + animation.currentStep

	//	priorKeyframe := int(math.Floor(float64(thisStep) / float64(keyframeSpacing)))
	priorKeyframe := int(math.Floor(float64(thisStep)/float64(keyframeSpacing))) - 1
	if priorKeyframe < 0 {
		priorKeyframe = 0
	}

	if priorKeyframe > len(animation.keyframes)-1 {
		fmt.Println("correcting prior keyframe", priorKeyframe)
		priorKeyframe = 0
	}

	//get the next keyframe in the array, if its greater than the length of the array roll back over to the starting frame
	nextKeyframe := priorKeyframe + 1
	if nextKeyframe > len(animation.keyframes)-1 {
		nextKeyframe = 0
	}

	if nextKeyframe > len(animation.keyframes)-1 {
		nextKeyframe = nextKeyframe - len(animation.keyframes) - 1
	}

	distanceBetweenFrames := nextKeyframe - priorKeyframe
	if distanceBetweenFrames < 0 {
		distanceBetweenFrames = int(float32(math.Abs(float64(distanceBetweenFrames))) - float32(len(animation.keyframes)-1))
	}

	fmt.Println("working on step:", thisStep, "steps:", steps, "currentStep:", animation.currentStep, "delta:", delta, "speed:", speed, "prior keyframe", priorKeyframe, "next keyframe:", nextKeyframe)

	biasPercentage := float32(steps) / float32(keyframeSpacing)

	//calculate the percentage between frames
	newPose := NewPose()
	for i := 0; i <= 5; i++ {
		for k := 0; k <= 2; k++ {
			pFrame := animation.keyframes[priorKeyframe].legs[i][k]
			nFrame := animation.keyframes[nextKeyframe].legs[i][k]

			distance := nFrame - pFrame
			progressionAmount := distance * biasPercentage
			newPose.legs[i][k] = pFrame + progressionAmount
		}
	}

	animation.currentStep = thisStep

	return newPose
}
