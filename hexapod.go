package main

import (
	"fmt"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/lx16a"
	"io"
	"time"
)

const (
	tickerMS      = 500
	servoAngleMin = 5
	servoAngleMax = 165
)

type Hexapod struct {
	Appendages            []Appendage
	configurationComplete bool
	serverConn            *csTools.ServerConnObj
	srl                   io.ReadWriteCloser
	currentAnimation      *Animation
	stopLoopChan          chan bool
	currentSpeed          float32
}

type Joint struct {
	offset float32
	Servo  *lx16a.Servo
}

type Appendage struct {
	Joints []Joint
}

func NewHexapod(network *lx16a.Network) *Hexapod {
	hexapod := &Hexapod{
		Appendages:   make([]Appendage, NUM_LEGS),
		stopLoopChan: make(chan bool),
	}
	for l := 0; l < NUM_LEGS; l++ {
		appendage := Appendage{
			Joints: make([]Joint, NUM_JOINTS),
		}
		for i := 1; i <= NUM_JOINTS; i++ {
			servoID := l*NUM_JOINTS + i
			appendage.Joints[i-1] = Joint{Servo: lx16a.NewServo(network, uint8(servoID))}
		}
		hexapod.Appendages[l] = appendage
	}
	return hexapod
}

func (hexapod *Hexapod) Startup() {
	hexapod.SetPose(&NeutralPose, 5*time.Second)

	//TODO: temporarily hacking in forward animation and setting speed
	hexapod.currentSpeed = 1
	forwardAnimation := NewForwardAnimation()
	hexapod.SetAnimation(forwardAnimation)
}

func (hexapod *Hexapod) SetPose(pose *Pose, duration time.Duration) {
	for k1, _ := range pose.legs {
		for k2, _ := range pose.legs[k1] {
			servoAngle := pose.legs[k1][k2]
			if servoAngle < servoAngleMin {
				servoAngle = servoAngleMin
			} else if servoAngle > servoAngleMax {
				servoAngle = servoAngleMax
			}
			hexapod.Appendages[k1].Joints[k2].Servo.TimedMove(servoAngle, uint16(duration))
		}
	}
}

func (hexapod *Hexapod) SetAnimation(animation *Animation) {
	hexapod.currentAnimation = animation
}

func (hexapod *Hexapod) stopLoop() {
	hexapod.stopLoopChan <- true
}

func (hexapod *Hexapod) startLoop() {
	currentTime := time.Now()
	ticker := time.NewTicker(tickerMS * time.Millisecond)

	for {
		select {
		case <-hexapod.stopLoopChan:
			return
		case _ = <-ticker.C:
			if hexapod.currentAnimation != nil {
				newPose := hexapod.currentAnimation.Update(time.Since(currentTime), hexapod.currentSpeed)
				//				hexapod.SetPose(newPose, time.Since(currentTime))
				fmt.Println("Setting pose:", newPose)
				hexapod.SetPose(newPose, 1)
			}
		}
		currentTime = time.Now()
	}
}
