package botclient

import (
	"gitlab.com/gamerscomplete/uuid"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
)

const (
	DEFAULT_TIMEOUT = 3
	SERVICE_NAME    = "hexapod"
)

type Client struct {
	conn *csTools.ServerConnObj
	addr uuid.UUID
}

func NewClient(conn *csTools.ServerConnObj, addr uuid.UUID) *Client {
	return &Client{
		conn: conn,
		addr: addr,
	}
}

func (client *Client) SetOffset(offset float32, servo int) (stats SystemStats, err error) {
	err = client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "SET_OFFSET", SERVICE_NAME, client.addr, nil, &stats)
	return
}

func (client *Client) GetSystemStats() (stats SystemStats, err error) {
	err = client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_SYSTEM_STATS", SERVICE_NAME, client.addr, nil, &stats)
	return
}

func (client *Client) GetServoStats(appendage int, joint int) (stats ServoStats, err error) {
	err = client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_SERVO_STATS", SERVICE_NAME, client.addr, ServoStatsRequest{Appendage: appendage, Joint: joint}, &stats)
	return
}

func (client *Client) GetAllServoStats() (stats []ServoStats, err error) {
	err = client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_ALL_SERVO_STATS", SERVICE_NAME, client.addr, nil, &stats)
	return
}

func (client *Client) GetServoConfiguration() (servoConfiguration ServoConfiguration, err error) {
	err = client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_SERVO_CONFIGURATION", SERVICE_NAME, client.addr, nil, &servoConfiguration)
	return
}
