package main

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	hexClient "gitlab.com/gamerscomplete/hexapod/client"
	"gitlab.com/gamerscomplete/lx16a"
	"io/ioutil"
)

func (hexapod *Hexapod) setupServerConn() error {
	hexapod.serverConn = csTools.NewServerConn()
	hexapod.serverConn.RegisterHandler("SET_POSE", hexapod.setPoseRequest)
	hexapod.serverConn.RegisterHandler("GET_SERVO_STATS", hexapod.getServoStatsRequest)
	hexapod.serverConn.RegisterHandler("GET_SYSTEM_STATS", hexapod.getSystemStatsRequest)
	hexapod.serverConn.RegisterHandler("GET_ALL_SERVO_STATS", hexapod.getAllServoStatsRequest)
	hexapod.serverConn.RegisterHandler("GET_SERVO_CONFIGURATION", hexapod.getServoConfigurationRequest)
	//	hexapod.serverConn.RegisterHandler("ENABLE_MOTOR", hexapod.enableMotorRequest)
	//	hexapod.serverConn.RegisterHandler("SET_JOINT_ANGLE", hexapod.setJointPositionRequest)
	//	hexapod.serverConn.RegisterHandler("SET_JOINT_ANGLES", hexapod.setJointPositionsRequest)
	if err := hexapod.serverConn.Init("hexapod"); err != nil {
		return err
	}
	return nil
}

func (hexapod *Hexapod) getServoConfigurationRequest(message csTools.ServerMessage) error {
	replyMessage := csTools.Message{Command: "REPLY"}

	reply := hexClient.ServoConfiguration{Configuration: make([][]uint8, len(hexapod.Appendages))}

	for k, v := range hexapod.Appendages {
		reply.Configuration[k] = make([]uint8, len(v.Joints))
		for k2, v2 := range v.Joints {
			reply.Configuration[k][k2] = v2.Servo.ID
		}
	}

	if err := replyMessage.PackType(reply); err != nil {
		return errors.New("failed to pack reply")
	}
	hexapod.serverConn.SendServerMessageReply(message, replyMessage)
	return nil
}

func (hexapod *Hexapod) getSystemStatsRequest(message csTools.ServerMessage) error {
	//	replyMessage := csTools.Message{Command: "REPLY"}
	//	reply := hexClient.SystemStats{}
	return errors.New("notImplemented")
}

func (hexapod *Hexapod) setPoseRequest(message csTools.ServerMessage) error {
	var request hexClient.SetPose
	if err := message.Data.UnpackType(&request); err != nil {
		log.Error(err)
		return errors.New("failed to unpack request")
	}

	return errors.New("setPoseRequest not implemented")

	/*
		for k1, v1 := range request.Pose {
			for k2, v2 := range v1 {
				hexapod
			}
		}
	*/
}

func (hexapod *Hexapod) getServoStatsRequest(message csTools.ServerMessage) error {
	replyMessage := csTools.Message{Command: "REPLY"}

	var request hexClient.ServoStatsRequest
	if err := message.Data.UnpackType(&request); err != nil {
		log.Error("failed to unpack servoStatsRequest: ", err)
		return errors.New("failed to unpack servoStats Request")
	}

	servo := hexapod.Appendages[request.Appendage].Joints[request.Joint].Servo
	stats, err := hexapod.getServoStats(servo)
	if err != nil {
		log.Error("Failed to get servo stat")
		return errors.New("Failed to get servo stats")
	}
	if err := replyMessage.PackType(*stats); err != nil {
		log.Error("Failed to pack reply", err)
		return errors.New("failed to pack reply")
	}
	hexapod.serverConn.SendServerMessageReply(message, replyMessage)
	return nil
}

func (hexapod *Hexapod) getAllServoStatsRequest(message csTools.ServerMessage) error {
	replyMessage := csTools.Message{Command: "REPLY"}

	reply := []hexClient.ServoStats{}

	for _, v := range hexapod.Appendages {
		for _, v2 := range v.Joints {
			stats, err := hexapod.getServoStats(v2.Servo)
			if err != nil {
				fmt.Println("Failed to get servo stats:", err)
				continue
			}
			reply = append(reply, *stats)
		}
	}

	if err := replyMessage.PackType(reply); err != nil {
		log.Error("Failed to pack reply", err)
		return errors.New("failed to pack reply")
	}
	hexapod.serverConn.SendServerMessageReply(message, replyMessage)
	return nil
}

func (hexapod *Hexapod) getServoStats(servo *lx16a.Servo) (*hexClient.ServoStats, error) {
	//TODO: This can easily be broken by sending out of range values.
	stats := &hexClient.ServoStats{ID: servo.ID}
	temp, err := servo.GetTemp()
	if err != nil {
		//TODO: handle what to do when one piece of the data is missing
		fmt.Println("Failed to get servo temp:", err)
		b, err2 := ioutil.ReadAll(hexapod.srl)
		if err != nil {
			log.Error("failed to clear searial buffer", err2)
		}
		log.Warnf("purged %d bytes", len(b))
		return nil, err
	}
	stats.Temp = temp

	voltage, err := servo.GetVoltage()
	if err != nil {
		//TODO: handle what to do when one piece of the data is missing
		fmt.Println("Failed to get servo voltage:", err)
		b, err2 := ioutil.ReadAll(hexapod.srl)
		if err != nil {
			log.Errorf("error purging serial buffer: %s\n", err2)
		}
		log.Infof("purged %d bytes", len(b))
		return nil, err
	}
	stats.Voltage = voltage

	angle, err := servo.GetAngle()
	if err != nil {
		//TODO: handle what to do when one piece of the data is missing
		fmt.Println("Failed to get servo angle:", err)
		b, err2 := ioutil.ReadAll(hexapod.srl)
		if err != nil {
			log.Error("error purging serial buffer", err2)
		}
		log.Infof("purged %d bytes", len(b))
		return nil, err
	}
	stats.Angle = angle

	/*
		mode, err := servo.GetMode()
		if err != nil {
			//TODO: handle what to do when one piece of the data is missing
			fmt.Println("Failed to get servo mode:", err)
			return
		}
		stats.Mode = mode
	*/
	motorEnabled, err := servo.MotorEnabled()
	if err != nil {
		//TODO: handle what to do when one piece of the data is missing
		fmt.Println("Failed to get servo motorEnabled:", err)
		b, err2 := ioutil.ReadAll(hexapod.srl)
		if err != nil {
			fmt.Println("error purging serial buffer: ", err2)
		}
		log.Infof("purged %d bytes", len(b))
		return nil, err
	}
	stats.MotorEnabled = motorEnabled
	return stats, nil
}
